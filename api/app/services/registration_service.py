from app import models
from app.services import token_service


def send_email_verification_mail(user: models.User) -> None:
    token = token_service.generate_verification_token({"user_id": user.id, "email": user.email})
    mail_service.send_verification_email(user.email, token)


def validate_verification_token(token) -> bool:
    payload = token_service.validate_and_decode(token)