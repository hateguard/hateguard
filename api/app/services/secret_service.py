import os

from passlib.context import CryptContext
from passlib.utils import generate_password

crypt_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def generate_random_str() -> str:
    return generate_password()


def generate_hash(string: str) -> str:
    return crypt_context.hash(string)


def is_same_secret(plain_string: str, hashed_string: str) -> bool:
    return crypt_context.verify(plain_string, hashed_string)
