from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from app import config


sg = SendGridAPIClient(config.MailConfig.SENDGRID_API_KEY)


def send_mail(self, template_id: config.MailConfig.TemplateID, to: str, dynamic_template_data: hash = None):
    message = Mail(from_email=config.MailConfig.FROM_ADDRESS, to_emails=to)
    message.dynamic_template_data = dynamic_template_data
    message.template_id = template_id.value
    self.sg.send(message)


def send_verification_email(self, to: str, token: str) -> None:
    self.send_mail(
        config.MailConfig.TemplateID.VERIFICATION_MAIL,
        to,
        {
            "token": token
        }
    )


def send_pw_reset_email(self, to: str, token: str) -> None:
    self.send_mail(
        config.MailConfig.TemplateID.PASSWORD_RESET_MAIL,
        to,
        {
            "token": token
        }
    )
