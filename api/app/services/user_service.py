from sqlalchemy.orm import Session
from app import models, schemas
from app.services import secret_service, registration_service


def get_user(db: Session, user_id: int) -> models.User:
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str) -> models.User:
    return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100) -> [models.User]:
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    db_user = models.User(email=user.email, hashed_password=secret_service.generate_hash(user.password))
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
