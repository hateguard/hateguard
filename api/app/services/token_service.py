import os

from app import models
from app.config.auth import AuthConfig
from app.config.app import AppConfig
from app.services import secret_service, user_service
import jwt
from datetime import timezone, datetime
from sqlalchemy.orm import Session, joinedload
from jwt.exceptions import *


def get_jwt(payload: dict) -> str:
    return jwt.encode(payload, os.getenv("JWT_KEY"), algorithm="HS256")


def generate_auth_token(user: models.User) -> str:
    now = datetime.now(tz=timezone.utc)

    return get_jwt({
        "user_id": user.id,
        "exp": now + AuthConfig.AUTH_TOKEN_EXPIRY,
        "iat": now,
        "iss": AppConfig.HOST,
        "aud": AppConfig.JWT_AUDIENCE,
        "type": "auth"
    })


def generate_refresh_token(user: models.User) -> str:
    now = datetime.now(tz=timezone.utc)

    return get_jwt({
        "user_id": user.id,
        "exp": now + AuthConfig.REFRESH_TOKEN_EXPIRY,
        "iat": now,
        "iss": AppConfig.HOST,
        "aud": AppConfig.JWT_AUDIENCE,
        "type": "refresh"
    })


def generate_id_token(user: models.User) -> str:
    now = datetime.now(tz=timezone.utc)

    return get_jwt({
        "user_id": user.id,
        "name": user.name,
        "exp": now + AuthConfig.AUTH_TOKEN_EXPIRY,
        "iat": now,
        "iss": AppConfig.HOST,
        "aud": AppConfig.JWT_AUDIENCE,
        "type": "id"
    })


def generate_verification_token(payload: hash) -> str:
    now = datetime.now(tz=timezone.utc)

    payload["type"] = "verification"
    payload["exp"] = now + AuthConfig.VERIFICATION_TOKEN_EXPIRY
    payload["iat"] = now
    payload["iss"] = AppConfig.HOST
    payload["aud"] = AppConfig.JWT_AUDIENCE
    return get_jwt(payload)


def validate_and_decode(token: str) -> hash:
    return jwt.decode(token, os.getenv("JWT_KEY"), algorithms=["HS256"], audience=AppConfig.JWT_AUDIENCE)


def generate_pw_reset_token(db: Session, user: models.User) -> str:
    """
    For password reset tokens we use the DB store because we need
    a higher level of protection against token re-use.
    :return: str
    """
    token_value = secret_service.generate_random_str()
    hashed_token = secret_service.generate_hash(token_value)
    token = models.Token(token_hash=hashed_token, user=user)
    db.add(token)
    db.commit()
    db.refresh(token)
    return f"{token.id}.{token_value}"


def validate_pw_reset_token(db: Session, token: str, email: str, auto_commit: bool = True) -> bool:
    token_id, token_value = token.split(".", 1)
    token = db.query(models.Token).options(joinedload(models.Token.user)).filter(models.Token.id == token_id).first()

    if not token or token.expires_at < datetime.now():
        return False
    if not secret_service.is_same_secret(token_value, token.token_hash):
        return False
    if token.user.email != email:
        return False

    db.delete(token)
    if auto_commit:
        db.commit()
    return True
