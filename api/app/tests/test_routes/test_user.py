import pytest

def test_create_user(client):
    response = client.post("/user", json={
        "email": "test@example.com",
        "password": "1234"
    })

    assert response.status_code == 200
    body = response.json()
    assert isinstance(body["id"], int)
    assert body["email"] == "test@example.com"
    with pytest.raises(KeyError):
        x = body["password"]
