import pytest
from unittest import mock
from app.config.mail import MailConfig

@pytest.fixture(scope="function")
def user(client):
    user = client.post("/user", json={
        "email": "test@example.com",
        "password": "1234"
    })
    return user.json()


@pytest.fixture(scope="function")
def token_response(client, user):
    return client.post("/auth/token", json={
        "grant_type": "password",
        "username": user["email"],
        "password": "1234"
    })


def test_get_token_with_password(client, user, token_response):
    response = token_response
    assert response.status_code == 200
    body = response.json()
    assert isinstance(body["auth_token"], str)
    assert isinstance(body["id_token"], str)
    assert isinstance(body["refresh_token"], str)


def test_get_token_with_refresh_token(client, user, token_response):
    response = client.post("/auth/token", json={
        "grant_type": "refresh_token",
        "client_secret": token_response.json()["refresh_token"],
        "client_id": user["id"]
    })
    assert response.status_code == 200
    body = response.json()
    assert isinstance(body["auth_token"], str)
    assert isinstance(body["id_token"], str)
    assert isinstance(body["refresh_token"], str)


# def test_password_reset_link_request(client, user, send_pw_reset_email_patch):
#     response = client.post("/auth/password-reset-link", json={
#         "email": user["email"],
#     })
#     assert response.status_code == 200
#     assert response.json()["ok"]
#     send_pw_reset_email_patch.assert_called_with(user["email"], mock.ANY)
