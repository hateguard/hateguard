from fastapi import Depends, HTTPException

from app.database import get_db
from sqlalchemy.orm import Session

from fastapi.security import OAuth2PasswordBearer
from app.services import token_service, user_service
from app.models import User


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)) -> User:
    try:
        payload = token_service.validate_and_decode(token)
    except token_service.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token Expired")
    except token_service.InvalidAudienceError:
        raise HTTPException(status_code=401, detail="Invalid Token Audience")
    except token_service.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid Token")
    except token_service.InvalidKeyError:
        raise HTTPException(status_code=500, detail="Invalid Key")

    db_user = user_service.get_user(db, payload["id"])

    if not db_user:
        raise HTTPException(status_code=401, detail="User does not exist")

    return db_user
