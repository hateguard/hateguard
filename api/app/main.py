from dotenv import load_dotenv
load_dotenv()

from fastapi import Depends, FastAPI
from fastapi.security import OAuth2PasswordBearer

from . import schemas
from .database import engine, Base
from .router import user, auth

Base.metadata.create_all(bind=engine)

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app.include_router(user.router)
app.include_router(auth.router)

# @app.get("/items/", response_model=list[schemas.Item])
# def read_items(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
#     items = crud.get_items(db, skip=skip, limit=limit)
#     return items
