from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    email_verified = Column(Boolean, default=False)

    items = relationship("Item", back_populates="owner")

    @property
    def name(self) -> str:
        return str(self.email)
