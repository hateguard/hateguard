import os


class PostgresConfig:
    HOST = os.getenv('POSTGRES_HOST')
    PORT = os.getenv('POSTGRES_PORT')
    PASSWORD = os.getenv('POSTGRES_PASSWORD')
    USER = os.getenv('POSTGRES_USER')
    DATABASE = os.getenv('POSTGRES_DB')

    @classmethod
    def url(cls) -> str:
        return f"postgresql://{cls.USER}:{cls.PASSWORD}@{cls.HOST}:{cls.PORT}/{cls.DATABASE}"


class TestPostgresConfig(PostgresConfig):
    DATABASE = os.getenv('POSTGRES_DB_TEST')