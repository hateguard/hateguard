import os


class AppConfig:
    HOST = os.getenv("HOST")
    JWT_AUDIENCE = os.getenv("JWT_AUDIENCE")
