import os
from enum import Enum


class MailConfig:
    SENDGRID_API_KEY = os.getenv("SENDGRID_API_KEY")
    FROM_ADDRESS = os.getenv("SENDGRID_FROM_ADDRESS")

    class TemplateID(Enum):
        VERIFICATION_MAIL = ""
        PASSWORD_RESET_MAIL = ""