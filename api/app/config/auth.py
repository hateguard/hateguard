from datetime import timedelta


class AuthConfig:
    AUTH_TOKEN_EXPIRY: timedelta = timedelta(hours=24)
    REFRESH_TOKEN_EXPIRY: timedelta = timedelta(days=60)
    VERIFICATION_TOKEN_EXPIRY: timedelta = timedelta(hours=1)
