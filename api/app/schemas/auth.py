from pydantic import BaseModel, Field

class OAuth2TokenRequestBase(BaseModel):
    grant_type: str
    client_id: str = None
    client_secret: str = None
    scope: str = None


class OAuth2PasswordRequest(OAuth2TokenRequestBase):
    """
    This is a class that recreates FastAPI's `OAuth2PasswordRequestFormStrict`
    """
    grant_type: str = Field(pattern="password")
    username: str
    password: str
    pass


class OAuth2RefreshRequest(OAuth2TokenRequestBase):
    grant_type: str = Field(pattern="refresh_token")
    client_id: str
    client_secret: str


class PasswordResetLinkRequestInput(BaseModel):
    email: str


class EmailVerificationRequestInput(BaseModel):
    token: str


class UpdatePasswordInputBase(BaseModel):
    new_password: str


class UpdatePasswordWithTokenInput(UpdatePasswordInputBase):
    email: str
    token: str


class UpdatePasswordInput(UpdatePasswordInputBase):
    old_password: str


class TokenSet(BaseModel):
    auth_token: str
    refresh_token: str
    id_token: str