from pydantic import BaseModel


class ActionResponse(BaseModel):
    ok: bool
