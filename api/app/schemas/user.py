from pydantic import BaseModel
from app.schemas.item import Item


class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    email_verified: bool
    items: list[Item] = []

    class Config:
        orm_mode = True