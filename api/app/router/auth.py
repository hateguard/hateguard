from typing import Union
from fastapi import APIRouter, Depends, HTTPException, BackgroundTasks
from sqlalchemy.orm import Session

from app import schemas, models
from app.services import user_service, token_service, secret_service, mail_service
from app.database import get_db
from app.dependencies.authentication import get_current_user

router = APIRouter(
    tags=['user', 'authentication'],
)


def verify_token(token) -> hash:
    try:
        payload = token_service.validate_and_decode(token)
    except token_service.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token expired")
    except token_service.InvalidAudienceError:
        raise HTTPException(status_code=401, detail="Invalid token audience")
    except token_service.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
    except token_service.InvalidKeyError:
        raise HTTPException(status_code=500, detail="Invalid key")
    return payload


@router.post("/auth/token", response_model=schemas.TokenSet)
def get_token(data: Union[schemas.OAuth2PasswordRequest, schemas.OAuth2RefreshRequest], db: Session = Depends(get_db)):
    if data.grant_type == "password":
        db_user = user_service.get_user_by_email(db, email=data.username)
        if not db_user or not secret_service.is_same_secret(data.password, db_user.hashed_password):
            raise HTTPException(status_code=401, detail="Email or Password incorrect")
    elif data.grant_type == "refresh_token":
        payload = verify_token(data.client_secret)
        if payload.get("type") != "refresh":
            raise HTTPException(status_code=401, detail="Not a refresh token")
        if str(payload.get("user_id")) != str(data.client_id):
            print(payload.get("user_id"))
            print(data.client_id)
            raise HTTPException(status_code=401, detail="Invalid Token for Client ID")

        db_user = user_service.get_user(db, payload.get("user_id"))

        if not db_user:
            raise HTTPException(status_code=401, detail="Invalid user ID")
    else:
        raise HTTPException(status_code=422, detail="Invalid Grant Type")

    return {
        "auth_token": token_service.generate_auth_token(db_user),
        "refresh_token": token_service.generate_refresh_token(db_user),
        "id_token": token_service.generate_id_token(db_user)
    }


@router.post("/auth/password-reset-link", response_model=schemas.ActionResponse)
def generate_password_reset_link(
        data: schemas.PasswordResetLinkRequestInput,
        background_tasks: BackgroundTasks,
        db: Session = Depends(get_db),
):
    db_user = user_service.get_user_by_email(db, data.email)

    token = token_service.generate_pw_reset_token(db, db_user)

    print('====================')
    print(db_user)
    print(token)

    background_tasks.add_task(mail_service.send_pw_reset_email, db_user.email, token)

    return {"ok": True}


@router.post("/auth/verify", response_model=schemas.ActionResponse)
def verify_email(data: schemas.EmailVerificationRequestInput, db: Session = Depends(get_db)):
    payload = verify_token(data.token)

    if payload["type"] != "verification":
        raise HTTPException(status_code=401, detail="Not a verification token")

    db_user = user_service.get_user(db, payload["user_id"])

    if payload["email"] != db_user.email:
        raise HTTPException(status_code=403, detail="This Email does not belong to this user")

    db_user.email_verified = True
    db.commit()

    return {"ok": True}


@router.put("/auth/reset/password", response_model=schemas.ActionResponse)
def update_password_with_reset_token(data: schemas.UpdatePasswordWithTokenInput, db: Session = Depends(get_db)):
    if not token_service.validate_pw_reset_token(db, data.token, data.email, auto_commit=False):
        raise HTTPException(status_code=401, detail="Invalid Token")

    db_user = user_service.get_user_by_email(db, data.email)
    db_user.hashed_password = secret_service.generate_hash(data.new_password)
    db.commit()

    return {"ok": True}


@router.put("/auth/password", response_model=schemas.ActionResponse)
def update_password(
        data: schemas.UpdatePasswordInput,
        user: models.User = Depends(get_current_user),
        db: Session = Depends(get_db)
):
    db_user = user_service.get_user(db, user.id)
    if not secret_service.is_same_secret(data.old_password, db_user.hashed_password):
        raise HTTPException(status_code=401, detail="Invalid Password")

    db_user.hashed_password = secret_service.generate_hash(data.new_password)
    db.commit()

    return {"ok": True}
