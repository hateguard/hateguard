# HateGuard

HateGuard is a project to build a web application that will connect to various DM services and provide a inbox view that filters messages considered hatespeech.

## Getting Involved

HateGuard is currently under active development. To check out ways to get involved, check ot the Issues page. Note that the development team is german, so we're using
german as a working language.

## Getting started

Clone this repository. Make sure docker is installed on your system, then run

```sh
make setup
docker compose up
```