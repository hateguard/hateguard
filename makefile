POSTGRES_PASSWORD := $(shell openssl rand -hex 12)

db:
	sed -i '' "s;^POSTGRES_PASSWORD=.*;POSTGRES_PASSWORD=$(POSTGRES_PASSWORD);g" .env

env:
	cp .env.sample .env; \
	sed -i '' "s;/path/to;$(shell pwd);g" .env

setup:
	make env && make db
